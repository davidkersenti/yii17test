<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $category
 * @property string $author
 * @property integer $statusId
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
     public function rules()
    {
        return [
            [['statusId', 'created_by', 'updated_by','author','category'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title','author'], 'string', 'max' => 255],
			[['body'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'statusId' => 'Status ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);
		
		//new record save to all else save only to updated fields
        if ($this->isNewRecord)
		{
			$this->created_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->created_by = Yii::$app->user->identity->id;
			$this->updated_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->updated_by = Yii::$app->user->identity->id;
			$this->author = Yii::$app->user->identity->id;
		}else{
			$this->updated_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->updated_by = Yii::$app->user->identity->id;
		}
		
		

        return $return;
    }	
		public static function getFindStatus($data)

    {
		return  Status::findOne($data)->status_name;
    }
	public static function getFindAuthor($data)
    {
		return  User::findOne($data)->username;
    }
	
	public static function getFindCategory($data)
	{
		return  Category::findOne($data)->category_name;
    }
}
