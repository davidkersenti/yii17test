<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_category`.
 */
class m170720_073010_create_table_category extends Migration
{
    /**
     * @inheritdoc
     */
     public function up()
    {
		$this->createTable('category', [
            'id' => $this->primaryKey(),
            'category_name' => $this->string(),
		]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_category');
    }
}
