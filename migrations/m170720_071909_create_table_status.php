<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_status`.
 */
class m170720_071909_create_table_status extends Migration
{
    /**
     * @inheritdoc
     */
     public function up()
    {
		$this->createTable('status', [
            'id' => $this->primaryKey(),
            'status_name' => $this->string(),
		]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_status');
    }
}
