<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_post`.
 */
class m170720_064625_create_table_post extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
		$this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'body' => $this->text(),
            'category' => $this->string(),
              'author' => $this->integer(),
		
            'statusId' => $this->integer(),
			
			'created_at' => $this->dateTime(),
			
			'updated_at' => $this->dateTime(),
		
			'created_by' => $this->integer(),
			
			'updated_by' => $this->integer(),
      ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_post');
    }
}
