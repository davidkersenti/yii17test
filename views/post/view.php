<?php

 use yii\helpers\Html;
 use yii\widgets\DetailView;
use app\models\Status;
use app\models\Category;
use app\models\User;
use app\models\Post;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body:ntext',
            
			            [
				'attribute' => 'category',
				'value' => Post::getFindCategory($model->category)
			],
            [
				'attribute' => 'author',
				'format' => 'html',
				'value' => Html::a(Post::getFindAuthor($model->author), ['user/view', 'id' => $model->author], ['format' => 'raw'])
			],
            [
				
				'attribute' => 'statusId',

				'value' => Post::getFindStatus($model->statusId)
			],
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
